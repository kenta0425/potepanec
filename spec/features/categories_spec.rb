RSpec.feature "Potepan::Categories", type: :feature do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, taxonomy: taxonomy) }
  given!(:product) { create(:product, taxons: [taxon]) }
  given(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  given!(:test_product) { create(:product, name: "test bag", taxons: [other_taxon]) }

  scenario "Confirm categories page" do
    visit potepan_category_path(taxon.id)
    expect(page).to have_title "#{taxon.name} | BIGBAG Store"

    within ".pageHeader" do
      expect(page).to have_selector 'h2', text: taxon.name
      expect(page).to have_link "Home", href: potepan_index_path
      expect(page).to have_selector 'a', text: "shop"
      expect(page).to have_selector 'li', text: taxon.name
    end

    within ".productBox" do
      expect(page).to have_link product.name, href: potepan_product_path(product.id)
      expect(page).to have_content product.display_price
      expect(page).not_to have_content "test bag"
    end

    within page.first ".panel-body" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_link taxon.name, href: potepan_category_path(taxon.id)
      expect(page).to have_content taxon.products.count
    end
  end
end
