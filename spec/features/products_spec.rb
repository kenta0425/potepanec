RSpec.feature "Potepan::Products", type: :feature do
  given(:taxon) { create(:taxon) }
  given!(:product) { create(:product, taxons: [taxon]) }
  given!(:related_product) { create(:product, taxons: [taxon]) }

  scenario "Confirm single product page" do
    visit potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} | BIGBAG Store"

    within ".pageHeader" do
      expect(page).to have_selector 'h2', text: product.name
      expect(page).to have_link "Home"
      expect(page).to have_selector 'li', text: product.name
    end

    within ".singleProduct" do
      expect(page).to have_css ".productSlider"
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
      expect(page).to have_link "一覧ページへ戻る"
    end

    within ".productsContent" do
      expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
      expect(page).to have_content related_product.display_price
    end

    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
