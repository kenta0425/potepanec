RSpec.describe Spree::Product, type: :model do
  describe 'related_product' do
    let(:taxon) { create(:taxon) }
    let(:other_taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:other_related_product) { create(:product, taxons: [other_taxon]) }

    it "product match related_product" do
      expect(product.related_products).to match_array related_product
    end

    it "product not include other_related_product" do
      expect(product.related_products).not_to include other_related_product
    end

    it "not include own product" do
      expect(product.related_products).not_to include product
    end
  end
end
