RSpec.describe "Potepan::Categories", type: :request do
  describe "Response Category page" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "renders the :show template" do
      expect(response).to render_template(:show)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "show taxonomy name" do
      expect(response.body).to include taxonomy.name
    end

    it "show taxon name" do
      expect(response.body).to include taxon.name
    end

    it "show product name" do
      expect(response.body).to include product.name
    end
  end
end
