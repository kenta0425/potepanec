RSpec.describe "Potepan::Products", type: :request do
  describe 'GET #show' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "renders the :show template" do
      expect(response).to render_template(:show)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "assigns instance variables" do
      expect(assigns(:product)).to eq product
    end

    it "show 4 related products" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
